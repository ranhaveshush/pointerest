package com.example.ranha.foursquarevenues.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ranha.foursquarevenues.R;
import com.example.ranha.foursquarevenues.model.dao.LocationDAO;
import com.example.ranha.foursquarevenues.model.dao.VenueDAO;

public class VenueDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_VENUE = VenueDetailsActivity.class.getName() + ".EXTRA_VENUE";

    private static final String STATE_VENUE = "STATE_VENUE";

    private VenueDAO m_venue;

    private View.OnClickListener m_actionButtonOnClickListener;

    public static void launch(Context context, VenueDAO venueDAO) {
        Intent intent = new Intent(context, VenueDetailsActivity.class);
        intent.putExtra(VenueDetailsActivity.EXTRA_VENUE, venueDAO);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_details);

        if (savedInstanceState != null) {
            m_venue = (VenueDAO) savedInstanceState.getSerializable(STATE_VENUE);
        } else {
            Intent intent = getIntent();
            m_venue = (VenueDAO) intent.getSerializableExtra(EXTRA_VENUE);
        }

        ImageView venueImage = (ImageView) findViewById(R.id.venue_image);
        Glide.with(venueImage.getContext())
                .load(m_venue.getPhotoUrl())
                .apply(new RequestOptions().centerCrop())
                .into(venueImage);

        ImageView backButton = (ImageView) findViewById(R.id.venue_back_button);
        backButton.setOnClickListener(v -> onBackPressed());

        TextView venueTitle = (TextView) findViewById(R.id.venue_title);
        venueTitle.setText(m_venue.getName());

        View.OnClickListener onClickListener = getActionButtonClickListener();
        initActionButton(R.id.call_action_button, R.drawable.action_call, R.string.action_call_text, onClickListener);
        initActionButton(R.id.navigate_action_button, R.drawable.action_navigate, R.string.action_navigate_text, onClickListener);
        initActionButton(R.id.browse_action_button, R.drawable.action_browse, R.string.action_browse_text, onClickListener);

        String formattedAddress = m_venue.getFormattedAddress(" - ");
        initDetailText(R.id.venue_address, formattedAddress);

        String hours = m_venue.getHoursText();
        initDetailText(R.id.venue_hours, hours);

        String category = m_venue.getCategory();
        initDetailText(R.id.venue_category, category);

        SpannableString price = m_venue.getPriceText();
        initDetailText(R.id.venue_price, price);

        String phone = m_venue.getPhone();
        initDetailText(R.id.venue_phone, phone);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STATE_VENUE, m_venue);

        super.onSaveInstanceState(outState);
    }

    private void initActionButton(int buttonId, int drawableResId, int textResId, View.OnClickListener onClickListener) {
        ImageView actionIcon;
        TextView actionText;
        ViewGroup browseActionButton = (ViewGroup) findViewById(buttonId);
        actionIcon = (ImageView) browseActionButton.findViewById(R.id.action_button_icon);
        actionIcon.setImageResource(drawableResId);
        actionIcon.getDrawable().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);
        actionText = (TextView) browseActionButton.findViewById(R.id.action_button_text);
        actionText.setText(textResId);
        browseActionButton.setOnClickListener(onClickListener);
    }

    private void initDetailText(int textViewResId, CharSequence charSequence) {
        TextView venuePriceAndHours = (TextView) findViewById(textViewResId);
        if (!TextUtils.isEmpty(charSequence)) {
            venuePriceAndHours.setText(charSequence);
            venuePriceAndHours.setVisibility(View.VISIBLE);
        } else {
            venuePriceAndHours.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener getActionButtonClickListener() {
        if (m_actionButtonOnClickListener != null) {
            return m_actionButtonOnClickListener;
        }

        m_actionButtonOnClickListener = v -> {
            Context context = v.getContext();
            switch (v.getId()) {
                case R.id.call_action_button: {
                    String phone = m_venue.getPhone();
                    if (TextUtils.isEmpty(phone)) {
                        Toast.makeText(context, R.string.toast_action_not_supported, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String uri = "tel:" + phone;
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                    launchIntent(context, intent);
                    break;
                }
                case R.id.navigate_action_button: {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    LocationDAO location = m_venue.getLocation();
                    if (location == null) {
                        Toast.makeText(context, R.string.toast_action_not_supported, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String formattedAddress = location.getFormattedAddress("+");
                    if (TextUtils.isEmpty(formattedAddress)) {
                        Toast.makeText(context, R.string.toast_action_not_supported, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String url = "geo:" + location.getLat() + "," +
                            location.getLng() + "?q=" + formattedAddress;
                    intent.setData(Uri.parse(url));
                    launchIntent(context, intent);
                    break;
                }
                case R.id.browse_action_button: {
                    String url = m_venue.getUrl();
                    if (TextUtils.isEmpty(url)) {
                        Toast.makeText(context, R.string.toast_action_not_supported, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    launchIntent(context, intent);
                    break;
                }
            }
        };

        return m_actionButtonOnClickListener;
    }

    private void launchIntent(Context context, Intent intent) {
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        if (componentName != null) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, R.string.toast_action_not_supported, Toast.LENGTH_SHORT).show();
        }
    }
}
