package com.example.ranha.foursquarevenues.model.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.io.Serializable;

/**
 * Created by ranha on 5/25/2017.
 */

public class BaseDAO implements Serializable {

    public BaseDAO() {
        super();
    }

    public static <T> T from(JsonElement data, Class<T> clazz) {
        return new Gson().fromJson(data, clazz);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this).toString();
    }
}
