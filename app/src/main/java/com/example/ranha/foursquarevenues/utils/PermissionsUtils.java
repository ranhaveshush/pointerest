package com.example.ranha.foursquarevenues.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

/**
 * Created by ranha on 6/14/2017.
 */

public class PermissionsUtils {

    private PermissionsUtils() {
    }

    public static boolean isLocationPermissionGranted(Activity activity) {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}
