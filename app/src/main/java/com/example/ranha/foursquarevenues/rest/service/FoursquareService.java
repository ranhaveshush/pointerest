package com.example.ranha.foursquarevenues.rest.service;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ranha on 5/24/2017.
 */

public interface FoursquareService {

    @GET("venues/explore")
    Call<JsonObject> getVenues(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") String version,
            @Query("ll") String latlang,
            @Query("radius") int radius,
            @Query("section") String sectionName,
            @Query("sortByDistance") int sortByDistance,
            @Query("venuePhotos") int venuePhotos,
            @Query("limit") int limit);
}
