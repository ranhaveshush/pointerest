package com.example.ranha.foursquarevenues.rest.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ranha.foursquarevenues.model.dao.VenueDAO;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ranha on 5/24/2017.
 */

public class FoursquareClient {

    private static final String TAG = "FoursquareClient";

    private static final String BASE_URL = "https://api.foursquare.com/v2/";
    private static final String CLIENT_ID = "M0DFSFGJURCQ1A3HFA4XR1AELAVUBML5JUMYPXKJLB0GGVFC";
    private static final String CLIENT_SECRET = "HRNZSQAVR0TBXR04UIOBP3BJSMKCGE3XTMZ44TZTW2QLXL1K";
    private static final String VERSION = "20170101";

    public static abstract class DataCallback<T> {

        public void onDone(T data) {
        }

        public void onError(Throwable t) {
        }
    }

    private static FoursquareService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FoursquareService service = retrofit.create(FoursquareService.class);
        return service;
    }

    public static void getVenues(String latlang, int radius, String sectionName, final DataCallback<List<VenueDAO>> callback) {
        Call<JsonObject> venuesCall = getService().getVenues(CLIENT_ID, CLIENT_SECRET, VERSION, latlang, radius, sectionName, 1, 1, 100);
        venuesCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (!response.isSuccessful()) {
                    if (callback != null) {
                        callback.onError(new IllegalStateException("Failed to get venues. code: " + response.code() + ", message: " + response.message()));
                    }
                    return;
                }

                JsonObject body = response.body();
                Log.i(TAG, body.toString());

                List<VenueDAO> venues = parseVenuesResponse(body);

                if (callback != null) {
                    callback.onDone(venues);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callback != null) {
                    callback.onError(t);
                }
            }
        });
    }

    @NonNull
    private static List<VenueDAO> parseVenuesResponse(JsonObject body) {
        List<VenueDAO> venues = new ArrayList<>();

        try {
            JsonObject responseObj = body.get("response").getAsJsonObject();
            JsonArray groupsArr = responseObj.get("groups").getAsJsonArray();
            for (JsonElement groupElem : groupsArr) {
                JsonObject groupObj = groupElem.getAsJsonObject();
                JsonArray itemsArr = groupObj.get("items").getAsJsonArray();
                for (JsonElement itemElem : itemsArr) {
                    JsonObject itemObj = itemElem.getAsJsonObject();
                    JsonObject venueObj = itemObj.get("venue").getAsJsonObject();
                    VenueDAO venue = VenueDAO.from(venueObj);
                    venues.add(venue);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse venues response:", e);
        }

        return venues;
    }
}
