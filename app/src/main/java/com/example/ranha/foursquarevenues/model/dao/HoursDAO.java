package com.example.ranha.foursquarevenues.model.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ranha on 5/27/2017.
 */

public class HoursDAO extends BaseDAO {

    @SerializedName("status")
    private String status;

    @SerializedName("isOpen")
    private Boolean isOpen;

    @SerializedName("isLocalHoliday")
    private Boolean isLocalHoliday;

    public HoursDAO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public Boolean getLocalHoliday() {
        return isLocalHoliday;
    }

    public void setLocalHoliday(Boolean localHoliday) {
        isLocalHoliday = localHoliday;
    }
}
