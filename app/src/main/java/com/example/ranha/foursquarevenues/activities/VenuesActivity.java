package com.example.ranha.foursquarevenues.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ranha.foursquarevenues.R;
import com.example.ranha.foursquarevenues.model.dao.LocationDAO;
import com.example.ranha.foursquarevenues.model.dao.PriceDAO;
import com.example.ranha.foursquarevenues.model.dao.VenueDAO;
import com.example.ranha.foursquarevenues.rest.service.FoursquareClient;
import com.example.ranha.foursquarevenues.utils.PermissionsUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;

public class VenuesActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = VenuesActivity.class.getSimpleName();

    private static final String EXTRA_SECTION_NAME = VenuesActivity.class.getName() + ".EXTRA_SECTION_NAME";

    private static final String STATE_SECTION_NAME = "STATE_SECTION_NAME";
    private static final String STATE_LAST_LOCATION = "STATE_LAST_LOCATION";
    private static final String STATE_LAST_POSITION = "STATE_LAST_POSITION";

    private static final String ADYEN_LATLANG = "52.376646,4.905965";
    private static final int RADIUS = 5000;

    private String m_sectionName;

    private GoogleApiClient m_googleApiClient;
    private Location m_lastLocation;
    private int m_lastPosition;

    private VenuesAdapter m_adapter;
    private StaggeredGridLayoutManager m_layoutManager;
    private RecyclerView m_recyclerView;
    private View m_oopsLayout;

    public static void launch(Context context, String sectionName) {
        Intent intent = new Intent(context, VenuesActivity.class);
        intent.putExtra(EXTRA_SECTION_NAME, sectionName);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // GoogleAPIClient instance creation,
        // to get the last known location
        if (m_googleApiClient == null) {
            m_googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(VenuesActivity.this)
                    .addOnConnectionFailedListener(VenuesActivity.this)
                    .addApi(LocationServices.API)
                    .build();
            m_googleApiClient.connect();
        }

        setContentView(R.layout.activity_venues);

        if (savedInstanceState != null) {
            m_sectionName = savedInstanceState.getString(STATE_SECTION_NAME);
            m_lastLocation = savedInstanceState.getParcelable(STATE_LAST_LOCATION);
            m_lastPosition = savedInstanceState.getInt(STATE_LAST_POSITION);
        } else {
            Intent intent = getIntent();
            m_sectionName = intent.getStringExtra(EXTRA_SECTION_NAME);
        }

        ImageView backButton = (ImageView) findViewById(R.id.venues_back_button);
        backButton.setOnClickListener(v -> onBackPressed());

        TextView categoryTitle = (TextView) findViewById(R.id.venues_category_title);
        categoryTitle.setText(m_sectionName);

        m_layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        m_recyclerView = (RecyclerView) findViewById(R.id.venues_list);
        m_recyclerView.setLayoutManager(m_layoutManager);

        m_oopsLayout = findViewById(R.id.venues_oops_layout);
        m_oopsLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STATE_SECTION_NAME, m_sectionName);
        outState.putParcelable(STATE_LAST_LOCATION, m_lastLocation);
        outState.putInt(STATE_LAST_POSITION, m_recyclerView.getScrollY());

        super.onSaveInstanceState(outState);
    }

    private void updateLocation(Location location) {
        if (location == null) {
            Log.wtf(TAG, "Location is null");
            return;
        }
        String latlang = location.getLatitude() + "," + location.getLongitude();
        FoursquareClient.getVenues(latlang, RADIUS, m_sectionName, new FoursquareClient.DataCallback<List<VenueDAO>>() {
            @Override
            public void onDone(List<VenueDAO> data) {
                if (data.size() <= 0) {
                    showOopsView();
                } else {
                    hideOopsView();
                    updateVenues(data);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);

                showOopsView();
            }
        });
    }

    private void showOopsView() {
        m_recyclerView.setVisibility(View.GONE);
        m_oopsLayout.setAlpha(0f);
        m_oopsLayout.setVisibility(View.VISIBLE);
        m_oopsLayout.animate().alpha(1f).start();
    }

    private void hideOopsView() {
        m_oopsLayout.setVisibility(View.GONE);
        m_recyclerView.setVisibility(View.VISIBLE);
    }

    private void updateVenues(List<VenueDAO> data) {
        m_adapter = new VenuesAdapter(VenuesActivity.this, data);
        m_recyclerView.setAdapter(m_adapter);
    }

    @Override
    protected void onStop() {
        if ((m_googleApiClient != null)
                && m_googleApiClient.isConnected()) {
            m_googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (m_lastLocation != null) {
            updateLocation(m_lastLocation);
            m_recyclerView.scrollTo(0, m_lastPosition);
        } else if (PermissionsUtils.isLocationPermissionGranted(VenuesActivity.this)) {
            m_lastLocation = LocationServices.FusedLocationApi.getLastLocation(m_googleApiClient);
            updateLocation(m_lastLocation);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        // TODO: handle connection suspended
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // TODO: launch Google play service required dialog
    }

    private static class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.VenueViewHolder> {

        private final LayoutInflater m_layoutInflater;
        private List<VenueDAO> m_venues;

        public VenuesAdapter(Context context, List<VenueDAO> venues) {
            super();

            m_layoutInflater = LayoutInflater.from(context);
            m_venues = venues;
        }

        @Override
        public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = m_layoutInflater.inflate(R.layout.venue_grid_item_layout, null, false);
            return new VenueViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(VenueViewHolder holder, int position) {
            VenueDAO venue = m_venues.get(position);
            LocationDAO location = venue.getLocation();
            PriceDAO price = venue.getPrice();

            holder.position = position;
            holder.venueNameText.setText(venue.getName());
            holder.venueCategoryText.setText(venue.getCategory());
            if (price != null) {
                holder.venuePriceText.setText(price.getText());
                holder.venuePriceText.setVisibility(View.VISIBLE);
            } else {
                holder.venuePriceText.setVisibility(View.GONE);
            }
            holder.venueDistanceText.setText(location.getDistanceText());
            holder.venueRatingText.setText(String.valueOf(venue.getRating()));
            String ratingColor = venue.getRatingColor();
            if (!TextUtils.isEmpty(ratingColor)) {
                holder.venueRatingText.getBackground().setColorFilter(Color.parseColor("#BB" + ratingColor), PorterDuff.Mode.SRC_OVER);
                holder.venueRatingText.setVisibility(View.VISIBLE);
            } else {
                holder.venueRatingText.setVisibility(View.GONE);
            }

            String photoUrl = venue.getPhotoUrl();
            if (!TextUtils.isEmpty(photoUrl)) {
                Glide.with(holder.venueImage.getContext())
                        .load(photoUrl)
                        .apply(new RequestOptions().centerCrop())
                        .into(holder.venueImage);
            }
        }

        @Override
        public int getItemCount() {
            return (m_venues == null) ? 0 : m_venues.size();
        }

        public class VenueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            int position;
            ImageView venueImage;
            TextView venueNameText;
            TextView venueCategoryText;
            TextView venuePriceText;
            TextView venueDistanceText;
            TextView venueRatingText;

            public VenueViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(this);
                venueImage = (ImageView) itemView.findViewById(R.id.venue_image);
                venueNameText = (TextView) itemView.findViewById(R.id.venue_name);
                venueCategoryText = (TextView) itemView.findViewById(R.id.venue_category);
                venuePriceText = (TextView) itemView.findViewById(R.id.venue_price);
                venueDistanceText = (TextView) itemView.findViewById(R.id.venue_distance);
                venueRatingText = (TextView) itemView.findViewById(R.id.venue_rating);
            }

            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                VenueDAO venueDAO = m_venues.get(position);
                VenueDetailsActivity.launch(context, venueDAO);
            }
        }
    }
}
