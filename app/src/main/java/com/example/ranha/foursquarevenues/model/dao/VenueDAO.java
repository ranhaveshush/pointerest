package com.example.ranha.foursquarevenues.model.dao;

import android.text.SpannableString;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranha on 5/24/2017.
 */

public class VenueDAO extends BaseDAO {

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private LocationDAO location;

    @SerializedName("contact")
    private ContactDAO contact;

    @SerializedName("price")
    private PriceDAO price;

    @SerializedName("hours")
    private HoursDAO hours;

    @SerializedName("url")
    private String url;

    @SerializedName("rating")
    private float rating;

    @SerializedName("ratingColor")
    private String ratingColor;

    @SerializedName("ratingSignals")
    private int ratingSignals;

    private String photoUrl;

    private String category;

    public VenueDAO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationDAO getLocation() {
        return location;
    }

    public PriceDAO getPrice() {
        return price;
    }

    public String getUrl() {
        return url;
    }

    public float getRating() {
        return rating;
    }

    public String getRatingColor() {
        return ratingColor;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFormattedAddress(String delimiter) {
        if (location == null) {
            return null;
        }
        return location.getFormattedAddress(delimiter);
    }

    public String getPhone() {
        if (contact == null) {
            return null;
        }
        return contact.getPhone();
    }

    public SpannableString getPriceText() {
        if (price == null) {
            return null;
        }
        return price.getText();
    }

    public String getHoursText() {
        if (hours == null) {
            return null;
        }
        return hours.getStatus();
    }

    public static VenueDAO from(JsonObject data) {
        VenueDAO venueDAO = new Gson().fromJson(data, VenueDAO.class);

        // Extract featured photo
        JsonElement featuredPhotosElem = data.get("featuredPhotos");
        if (featuredPhotosElem != null) {
            JsonObject featuredPhotos = featuredPhotosElem.getAsJsonObject();
            int count = featuredPhotos.get("count").getAsInt();
            if (count > 0) {
                JsonArray groupsArr = featuredPhotos.get("items").getAsJsonArray();
                JsonObject groupObj = groupsArr.get(0).getAsJsonObject();
                String prefix = groupObj.get("prefix").getAsString();
                String suffix = groupObj.get("suffix").getAsString();
                int width = groupObj.get("width").getAsInt();
                int height = groupObj.get("height").getAsInt();
                String photoUrl = prefix + width + "x" + height + suffix;
                venueDAO.setPhotoUrl(photoUrl);
            }
        }

        // Extract category
        JsonArray categoriesArr = data.get("categories").getAsJsonArray();
        for (JsonElement categoryElem : categoriesArr) {
            JsonObject categoryObj = categoryElem.getAsJsonObject();
            boolean isPrimary = categoryObj.get("primary").getAsBoolean();
            if (isPrimary) {
                String categoryName = categoryObj.get("shortName").getAsString();
                venueDAO.setCategory(categoryName);
            }
        }

        return venueDAO;
    }
}
