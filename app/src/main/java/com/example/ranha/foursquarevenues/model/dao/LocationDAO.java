package com.example.ranha.foursquarevenues.model.dao;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ranha on 5/25/2017.
 */

public class LocationDAO extends BaseDAO {

    @SerializedName("lat")
    private String lat;

    @SerializedName("lng")
    private String lng;

    @SerializedName("distance")
    private int distance;

    @SerializedName("address")
    private String address;

    @SerializedName("city")
    private String city;

    public LocationDAO() {
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistanceText() {
        if (distance >= 1000) {
            return String.format("%.2f", (distance / 1000D)) + "k";
        } else {
            return distance + "m";
        }
    }

    public String getFormattedAddress(String delimiter) {
        String formattedAddress = null;
        if (!TextUtils.isEmpty(address)) {
            formattedAddress = address;
        }
        if (!TextUtils.isEmpty(city)) {
            if (!TextUtils.isEmpty(formattedAddress)) {
                formattedAddress += delimiter + city;
            } else {
                formattedAddress = city;
            }
        }
        return formattedAddress;
    }
}
