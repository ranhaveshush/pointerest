package com.example.ranha.foursquarevenues.activities;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ranha.foursquarevenues.R;
import com.example.ranha.foursquarevenues.utils.PermissionsUtils;

/**
 * Created by ranha on 5/27/2017.
 */

public class SectionsActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_LOCATION_PERMISSION = 1001;

    private ViewGroup m_requestPermissionsLayout;
    private RecyclerView m_sectionsRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sections);

        m_requestPermissionsLayout = (ViewGroup) findViewById(R.id.request_permissions_layout);

        String[] sectionsNames = getResources().getStringArray(R.array.venues_sections_names);

        SectionsAdapter sectionsAdapter = new SectionsAdapter(SectionsActivity.this, sectionsNames);
        GridLayoutManager layoutManager = new GridLayoutManager(SectionsActivity.this, 3);
        m_sectionsRecyclerView = (RecyclerView) findViewById(R.id.main_venues_sections_list);
        m_sectionsRecyclerView.setLayoutManager(layoutManager);
        m_sectionsRecyclerView.setAdapter(sectionsAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (PermissionsUtils.isLocationPermissionGranted(SectionsActivity.this)) {
            m_requestPermissionsLayout.setVisibility(View.GONE);
            m_sectionsRecyclerView.setVisibility(View.VISIBLE);
        } else {
            m_requestPermissionsLayout.setVisibility(View.VISIBLE);
            m_sectionsRecyclerView.setVisibility(View.GONE);
            View grantPermissionsButton = m_requestPermissionsLayout.findViewById(R.id.grant_permissions_button);
            grantPermissionsButton.setOnClickListener(v -> requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION));
        }
    }

    private static class SectionsAdapter extends RecyclerView.Adapter<SectionsAdapter.SectionViewHolder> {

        private final Context m_context;
        private LayoutInflater m_layoutInflater;
        private String[] m_sectionNames;

        public SectionsAdapter(Context context, String[] sectionNames) {
            super();

            m_context = context;
            m_layoutInflater = m_layoutInflater.from(context);
            m_sectionNames = sectionNames;
        }

        @Override
        public SectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = m_layoutInflater.inflate(R.layout.venue_section_grid_item_layout, null, false);
            return new SectionViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(SectionViewHolder holder, int position) {
            String sectionName = m_sectionNames[position];
            String sectionIconName = sectionName.toLowerCase();
            int sectionIconResId = m_context.getResources().getIdentifier(sectionIconName, "drawable", m_context.getPackageName());

            holder.sectionName = sectionName;
            holder.sectionText.setText(sectionName);
            if (sectionIconResId != 0) {
                holder.sectionImage.setImageResource(sectionIconResId);
                holder.sectionImage.setVisibility(View.VISIBLE);
            } else {
                holder.sectionImage.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return (m_sectionNames != null) ? m_sectionNames.length : 0;
        }

        public static class SectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            String sectionName;
            ImageView sectionImage;
            TextView sectionText;

            public SectionViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(this);
                sectionImage = (ImageView) itemView.findViewById(R.id.venue_section_icon);
                sectionText = (TextView) itemView.findViewById(R.id.venue_section_text);
            }

            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                VenuesActivity.launch(context, this.sectionName);
            }
        }
    }
}
