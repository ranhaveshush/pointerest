package com.example.ranha.foursquarevenues.model.dao;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ranha on 5/27/2017.
 */

public class PriceDAO extends BaseDAO {

    @SerializedName("tier")
    private int tier;

    @SerializedName("message")
    private String message;

    @SerializedName("currency")
    private String currency;

    public PriceDAO() {
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public SpannableString getText() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i < 5; ++i) {
            sb.append(currency);
        }
        SpannableString ss = new SpannableString(sb.toString());
        ss.setSpan(new ForegroundColorSpan(0xCC000000), 0, tier, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(0x44000000), tier, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}