package com.example.ranha.foursquarevenues.model.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ranha on 5/25/2017.
 */

public class ContactDAO extends BaseDAO {

    @SerializedName("phone")
    private String phone;

    public ContactDAO() {
        super();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
